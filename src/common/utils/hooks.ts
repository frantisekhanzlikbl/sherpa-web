import { useEffect, useRef } from "react";

export const useUnmountHandler = (handler: () => void) => {
	// to keep the `useEffect` from firing all the time but still allowing to use state on the handler,
	// the handler has to be stored in a ref (making it referentially stable).
	const handlerRef = useRef(handler);

	// when the passed handler changes, update the ref so that the handler captures the proper state.
	useEffect(() => {
		handlerRef.current = handler;
	}, [handler]);

	useEffect(
		() => () => {
			handlerRef.current();
		},
		[],
	);
};
