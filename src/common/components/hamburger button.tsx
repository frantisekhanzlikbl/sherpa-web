import { css } from "@emotion/react";
import styled from "@emotion/styled";

export const HamburgerButton = ({
	open,
	setOpen,
}: {
	open: boolean;
	setOpen: (_open: boolean) => void;
}) => (
	<Container onClick={() => setOpen(!open)}>
		<BarTop open={open} />
		<BarMiddle open={open} />
		<BarBottom open={open} />
	</Container>
);

const Container = styled.button`
	width: 4rem;
	height: 4rem;

	display: flex;
	flex-direction: column;
	justify-content: space-evenly;
`;

const bar = css`
	width: 100%;
	height: 15%;

	border-radius: 1rem;
	background-color: #000;

	transition: 0.5s transform;
`;

const BarTop = styled.div<{ open: boolean }>`
	${bar}

	/* x axis must be in the middle of the border curvature, which is r=d/2 away from the left edge and since d is the bar height we just halve its height */
	transform-origin: 7.5% 50%;

	${({ open }) =>
		open
			? css`
					transform: rotateZ(50grad);
			  `
			: ""}
`;

const BarMiddle = styled.div<{ open: boolean }>`
	${bar}

	${({ open }) =>
		open
			? css`
					transform: scaleX(0);
			  `
			: ""}
`;

const BarBottom = styled.div<{ open: boolean }>`
	${bar}

	/* x axis must be in the middle of the border curvature, which is r=d/2 away from the left edge and since d is the bar height we just halve its height */
	transform-origin: 7.5% 50%;

	${({ open }) =>
		open
			? css`
					transform: rotateZ(-50grad);
			  `
			: ""}
`;
