type Color = string;

export interface Theme {
	colors: {
		foreground: {
			normal: Color;
			modal: Color;

			primary: Color;
			secondary: Color;

			accept: Color;

			//////////////////////////////////////////////////
			// non-contextual colors                        //
			//                                              //
			// to be used for example to overlay text       //
			// on an image that does not change with theme. //
			//////////////////////////////////////////////////

			white: Color;
		};
		background: {
			normal: Color;
			modal: Color;

			accept: Color;

			//////////////////////////////////////////////////
			// non-contextual colors                        //
			//                                              //
			// to be used for example to overlay text       //
			// on an image that does not change with theme. //
			//////////////////////////////////////////////////

			white: Color;
		};
		scrollbar: {
			track: Color;
			thumb: Color;
		};
	};
}

const palette = {
	black: "hsl(0, 0%, 0%)",
	gray0: "hsl(0, 0%, 40%)",
	white: "hsl(0, 0%, 100%)",
	blueLight: "hsl(191.3, 100%, 50%)",
	blue: "hsl(210.8, 100%, 50%)",
	green: "hsl(101.3, 91.1%, 44.1%)",
};

export const themes: Record<"light", Theme> = {
	light: {
		colors: {
			foreground: {
				normal: palette.black,
				modal: palette.white,
				primary: palette.blueLight,
				secondary: palette.blue,
				accept: palette.white,
				white: palette.white,
			},
			background: {
				normal: palette.white,
				modal: palette.black,
				accept: palette.green,
				white: palette.white,
			},
			scrollbar: {
				track: palette.white,
				thumb: palette.gray0,
			},
		},
	},
};
