export type CssProps = {
	display: "block" | "flex" | "inline" | "inline-block";
};
